"""Spirograph"""

import random
from turtle import Screen, Turtle

timmy = Turtle()
screen = Screen()
angle = 5
timmy.color("blue")
timmy.speed(0)

for _ in range(0, 360, angle):
    timmy.pencolor(random.random(), random.random(), random.random())
    timmy.circle(100)
    timmy.left(angle)

screen.exitonclick()
