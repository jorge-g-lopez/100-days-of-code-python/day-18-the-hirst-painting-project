"""Draw a dashed line"""

from turtle import Screen, Turtle

timmy = Turtle()
screen = Screen()

timmy.shape("turtle")
timmy.color("blue")

for _ in range(10):
    timmy.pendown()
    timmy.forward(10)
    timmy.penup()
    timmy.forward(10)

screen.exitonclick()
