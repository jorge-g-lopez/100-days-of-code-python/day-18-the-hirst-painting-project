"""Random walk"""

import random
from turtle import Screen, Turtle

timmy = Turtle()
screen = Screen()

timmy.color("blue")
timmy.speed(0)
timmy.width(5)

while True:
    timmy.pencolor(random.random(), random.random(), random.random())
    angle = 90 * random.randint(1, 4)
    timmy.forward(20)
    timmy.left(angle)
