"""Draw a triangle, square, pentagon, hexagon,
heptagon, octagon, nonagon and decagon"""

from turtle import Screen, Turtle
import random

timmy = Turtle()
screen = Screen()

timmy.shape("turtle")
timmy.color("blue")

# Position the turtle lower in the screen

timmy.right(90)
timmy.penup()
timmy.forward(100)
timmy.pendown()
timmy.left(90)

for sides in range(3, 11):
    timmy.pencolor(random.random(), random.random(), random.random())
    angle = 360 / sides

    for _ in range(sides):
        timmy.forward(100)
        timmy.left(angle)

screen.exitonclick()
