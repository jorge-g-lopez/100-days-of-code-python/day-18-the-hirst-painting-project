"""The Hirst Painting Project"""

import random
from turtle import Screen, Turtle

import colorgram

IMAGE = "project/image.jpg"
xcor = -280
ycor = -330
STEP = 60
SIZE = 10
DOTSIZE = 20
timmy = Turtle()
screen = Screen()
screen.colormode(255)
timmy.color("blue")
timmy.speed(0)
rgb_colors = []
colors = colorgram.extract(IMAGE, 30)

for color in colors:
    new_color = (color.rgb.r, color.rgb.g, color.rgb.b)
    rgb_colors.append(new_color)

timmy.penup()
timmy.hideturtle()

for _ in range(SIZE):
    ycor += STEP
    timmy.sety(ycor)
    timmy.setx(xcor)

    for _ in range(SIZE):
        timmy.pencolor(random.choice(rgb_colors))
        timmy.dot(DOTSIZE)
        timmy.forward(STEP)

screen.exitonclick()
